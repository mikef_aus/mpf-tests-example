#!/bin/sh
if [ -f /proc/1/cgroup ] && [ $(grep -c docker /proc/1/cgroup) -gt 0 ]; then
  python3 -m unittest
else
  docker run -v `pwd`:/mpf -it mfuller/mpf-docker /mpf/test.sh
fi

